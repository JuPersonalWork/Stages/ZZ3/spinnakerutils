How to use that VM
===

It depends on what you want to do with the SpiNNaker board. That VM allows you to
interact with a SpiNNaker board with the high level Python API : sPyNNaker, or
with low level tools (ybug, tubotron, ...) and C API.

# Low level API

All those tools are in /home/vagrant/.local/share/SPINNAKER-TOOLS/tools

The main one is ybug which allows you to communicate with the board under TCP or
UDP protocol. If you want to connect to the board and run some applications
or monitor the board state you should use the following command:

> ybug \<board main ip or domain name\>

If you want to use the bmp protocol to power on/off computes core or reset them
you have to use a dedicated parameter:

> ybug \<board main ip or domain name\> -bmp \<board bmp ip or domain name\>/0

If you want more information about how to use that tool, see the following
directory: /home/vagrant/.local/share/SPINNAKER-TOOLS/docs

For low level C API look into the same directory.

# High level API

The Python high level API is installed into a virtualenv :
/home/vagrant/SpiNNaker_virtualenv

You can find an example to run and check that everything works fine into
/home/vagrant/WIP/PyNNExamples-3.0.0/examples/

> cd /home/vagrant/WIP/PyNNExamples-3.0.0/examples/
> python va_benchmark.py

You should see the following output :

![](Figure_1.png)

# Useful aliases in the VM :

__maj__: To run "apt-get update" and "apt-get upgrade" to update Debian packages.</br>
__ll__: To list all files in the current directory with details.</br>
__pynnup__: To activate the sPyNNaker virtualenv to run sPyNNaker
python scripts. Type 'deactivate' to disable the virtualenv.</br>
