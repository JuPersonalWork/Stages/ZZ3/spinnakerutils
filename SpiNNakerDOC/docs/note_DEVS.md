 # Notes sur Devs

Discrete Event System Specification.

DEVS est un formalisme modulaire et hierarchique pour la modélisation , la
simulation et l'analyse de systèmes complexes qui peuvent être des systèmes à
événements discrets décrits par des fonctions de transitions d'états et des
systèmes continus décrits par des équations différentielles, par exemple des
systèmes hybrides (continus et discrêts).
[https://fr.wikipedia.org/wiki/Discrete_Event_System_Specification](https://fr.wikipedia.org/wiki/Discrete_Event_System_Specification)

## I - historique

Inventé par Bernard P. Zeigler, professeur émérite de l'université d'Arizona.
DEVS à été présenté au publique en 1976. Ce formalisme peut être vu comme une
extenstion du formalisme de la machine de Moore. Il s'agit dún automate fini dans
lequel les sorties sont déterminées uniquement par rapport à l'état du système
et non de l'entrée en elle-même. L'extension de ce formalisme consiste ici en
deux ajouts :
* l'association d'une durée de vie à chaque état (nombre réel positif qui peut
  être déterminé aléatoirement suivant une loi de distribution de probabilitée)
* un concept de hierarchie grâce au couplage

En 1984, Zeigler a proposé un algorithme hierarchique pour la simulation de
modèles DEVS qui a été publié en 1987 dans le journal _Simulation_. de nombreux
formalismes étendant DEVS on vu le jour depuis lors : DESS/DEVS, P-DEVS pour
parallel DEVS, ...

De par sa nature modulaire et sa hierarchie de modèles, ainsi que de sa capacité
d'analyse basée sur la simulation, le formalisme DEVS et ses variations ont été
utilisée dans de nombreuses applications d'ingénierie et de science :
* hardware design
* système de communication
* biologie
* sociologie
* ...

<div class="page-break"></div>

## II - Formalism



<div class="page-break"></div>

## Definitions

### DEVS

Discrete Event System Specification

### Hardware

Matériel informatique physique.

### Machine de Moore

En informatique théorique, notamment en théorie des automates, et en théorie de
la calculabilité, une machine de Moore ou automate de Moore (proposée par
Edward F. Moore) est un automate fini (et plus précisément un transducteur fini)
pour lequel les sorties ne dépendent que de l'état courant. Cela signifie que
chaque état est doté d'une lettre de sortie. La lettre est émise lorsque l'état
est atteint. En particulier, la longueur du mot de sortie est égale à la
longueur du mot d'entrée.

Cette définition est plus restrictive que celle des machines de Mealy pour
lesquelles les valeurs de sortie dépendent à la fois de l'état courant et de la
lettre d'entrée. Toutefois, il existe pour chaque machine de Moore, une machine
de Mealy équivalente et réciproquement.

Les machines de Moore constituent la famille la plus simple de transducteurs finis.
