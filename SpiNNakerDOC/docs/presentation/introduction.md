# The SpiNNaker project

## Beginning

The SpiNNaker project began in 2005 thanks to Engineering and Physical Sciences
Research Council (EPSRC). It assemble several universities (Manchester,
Southampton, Cambridge, Sheffield) and industrial partners (ARM Ltd, Silistix
and Thales). Each of them had its task:


* __Manchester University__: design and develop the SpiNNaker machine hardware
architecture via the Advanced Processor Technologies Research Group (APT) partly
founded by [Steve FURBER](http://apt.cs.manchester.ac.uk/people/sfurber/), a
huge SpiNNaker project's collaborator.
* __Southampton University__: setup and management of user problems tracking
software and SpiNNaker working demonstration's development directly with
Sheffield University.
* __Cambridge University__: development of a SpiNNaker alternative solution on
Field-Programmable Gate Array (FPGA) card in order to be a SpiNNaker performance
control machine.
* __Sheffield University__: SpiNNaker working demonstration's development
directly with Southampton University.
* __ARM Ltd__: provide the SpiNNaker base computing unit : ARM 968 core.
* __Silistix Ltd__: provide the asynchronous interconnection system between ARM
cores, packet router and SpiNNaker chip external memory.

The first SpiNNaker chip prototype was delivered in 2009 and was fully
functional. it was followed two years later in 2011 by the current SpiNNaker
machine composed of 48 SpiNNaker chip. It's purpose is to be the base block
of a big neuromorphic simulator designed to simulate 1% of the Human Brain
neuron count. One card of that version might be interconnected with 8 other
same type card to create a network of SpiNNaker card. The Manchester University
build a cluster composed of 600 of those SpiNNaker board as you can see on the
[following picture](#spinn5cluster).

![](../img/spinn5_cluster.jpg){: #spinn5cluster}

The SpiNNaker machine is accessible by researchers around the world since 2012.

Today, SpiNNaker is a part of a bigger project that is European-wide project :
the Human Brain Project (HBP). Researchers have two way to use a SpiNNaker
machine. The first one is to buy a card for anexclusive use but it might be
expensive (some thousand's of €). The second way is to require an account on the
HBP platform and then submit jobs as Python simulations script for the Manchester
University SpiNNaker cluster.

## Human Brain Project

The [HBP](https://www.humanbrainproject.eu/en/) was born in 2013 from a European
initiative. It's supposed to last 10 years. It wasn't started by biologists but
by the Information and Communication Technology (ICT). It's purpose is to
accelerate research around  neurosciences, computing and neurobiologie by
assemble a wide field of researchers groups : hardware architecture designers,
neurobilogists, developers, ... The goal is to strive for a better Human Brain
understanding.

The HBP is divided into several pillars including Brain Simulation and Silicon
Brain. SpiNNaker project is part of the Silicon Brain pillar. It's purpose is to
be use in neurosciences to increase brain understanding, in robotics to drive
robots with a low energy consumption computing unit simulating logical neural
networks, or in computer science to strive to break modern super computing rules.

## Future

The SpiNNaker project has not reach is end yet. During the [BioComp Summer School
2017](http://gdr-biocomp.fr/en/ecole-biocomp-2017/), Steve FURBER has made a talk
about SpiNNaker, how it works and where it goes. He explained that Teams that
have created the first version got now hefty fundings. That's why they are now
working on a second version of the Hardware in which they will fix the first
versions main issues that are memory space, cores computing power, ...
