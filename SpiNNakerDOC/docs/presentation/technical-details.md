# A simulator

The SpiNNaker machine is a neuromorphic simulator. Nowadays it exist two big
simulators which are custom hardware and custom software simulators. In the
first case the simulation is directly done by an hardware architecture like
FPGA card. In the second case, the simulation is achieve by a software specific
simulator which is design to run on standard computers. In fact SpiNNaker is
both at the same time. It's hardware is specifically design for neural
simulation (memory size, cores interconnections, ...) and the software is
optimize to run neural simulations on this hardware. The SpiNNaker neuromorphic
simulator is build in 3 layer which are:

* hardware layer
* low level software layer in C
* high level software layer in Python

# SpiNNaker principles

Informations that are given there concern the full SpiNNaker simulator, that
is to say the Hardware and the Software.

SpiNNaker does not respect standard high performance computing principles that
are memory consistency, synchronization and determinism. SpiNNaker principles
are following:

* __memory inconsistency__: each core is able to alter memory without notifying
or alert other cores. Only locale data are known as in biology. Cores has two
kind of memory : their tightly coupled locale memory and the ship Synchronous
Random Access Memory (SDRAM). The core is the only one able to use it's locale
memory while the SDRAM is accessible to all the ship's cores. The SDRAM allow
information sharing between cores. For instance it can be a spike that should
be propagate from a cores to another. Furthermore, each logical synapse is
associated with the post-synaptic neuron of the connection and may connect many
input neuron to it's output one. To simplify this behavior, the shared memory
in the SDRAM is spit into many discrete area, one for each logical synapse. In
that way, the SDRAm is more like an extension of the cores local memory. In the
core view point, every memory is locale.
* __non-determinism__: each chip is Globally Asynchronous, Locally Synchronous,
that is to say each core that compose the chip is synchronous, use it's own
clock et communicate with others cores asynchronously.
* the simulator use an low level event driven model, that is to say just under
the software simulator. There isn't any function execution control but there is
a callback system. Spikes are events, send from a neuron as a message that
specify the sender and the receiver. Each event type as a list of associated
functions with priority that are executed following their priority. When an
event happen all associated functions are executed.
There is two type of callbacks function that are queueable and non-queueable
functions. Depending on the type of callback that is raise the scheduler run the
function immediately and atomically or put it into the callback queue thanks to
it's priority (cf. [Following picture](#eventDriveModel)). When the running callback end it's task,
the next one in the queue start. Queueable callbacks execution isn't necessarily
atomic. Indeed, when an event that happen imply a non-queueable callback during
a queueable callback execution, this last one is temporarily stopped to let the
non-queueable callback make it's task until this end. If the task queue is empty
the dispatcher go in sleep mode until a new event happen to save some energy.

![test](../img/spinnaker_event_drive.svg){: #eventDriveModel}

* the simulation run in real time. The project goal is to simulate 1% of the
human brain in real time, what's need around 50 000 SpiNNaker chip. It explain
why we enable a little packet lost. In that way the model is closer from reality
in which some signals are lost without being transmit to the brain.

# Hardware Architecture

We should begin with some numbers. The I3S Laboratory's SpiNNaker machine is a
48 node board called Spinn5 board. It is composed of 48 SpiNNaker chip each of
which contain 18 ARM 968 core which make 864 cores throughout the machine. That
is why SpiNNaker is called Many Core machine.

## Chip

The [following image](#chipPicture) is a SpiNNaker chip's picture on which we can clearly
distinguish each of it's components.

![](../img/spinn_labeled_bw.png){: #chipPicture}

This chip measure 10 mm X 9.7 mm. It include a packet router in it's center that
is in charge of manage messages that are internal, from the outside or to the
outside of the chip. Those messages are low level messages in the low level
event drive model.

Around that router are dispatch the 18 cores. Those cores have a modified
Harvard architecture. They are composed of 3 parts which are the computing
unit, the Instruction Tightly-Coupled Memory (ITCM) and the  Data Tightly-Coupled
Memory (DTCM). The ITCM contain all instructions that should be executed by the
computing unit. The DTCM contain all data that are manipulated the computing
unit. Those memory are rather small : 32 KB for the ITCM and 64 KB for the DTCM.
However memory access in ITCM and DTCM is fast, around 1 clock cycle (5 ns)
while memory access in SDRAM and System RAM (SysRAM) are 20 to 30 times slower.

The reason behind ARM 968 cores usage is to face a huge constraint for such
wide simulators and large scale computation. This is the energy consumption.
Indeed it's absolutely necessary that each core has a low energy consumption
which is true for the ARM 968 core (from 0.12 mW/MHz to 0.23mW/MHz). But those
cores got two drawbacks which are a limited memory and no floating number
representation. Indeed the SpiNNaker software layer use real numbers. There is
a SpiNNaker chip version that allow floating point but it has a higher energy
consuption ([see this](https://studentnet.cs.manchester.ac.uk/resources/library/thesis_abstracts/MSc12/FullText/Moise-Mircea-fulltext.pdf)).

Let's go back to the SpiNNaker chip. There is two memory on the SpiNNaker chip
which are the SDRAM (128 MB) and the SysRAM (32 KB). They allow to stored
data that should be shared between all chip's cores. There is a last memory
type on a SpiNNaker chip which is read-only. This the Read Only Memory (ROM).
It contains the software that should be load on the monitoring core of the
SpiNNaker chip.  

## Board

### Work force

On the [following schemas](#spinn5BoardSchemas) we can see the SpiNNaker 48 node
board composition. It is composed of 48 SpiNNaker chip that are interconnected
in an hexagonal structure. Each chip is connected to 6 of its neighbors with a
bidirectional link. Chips are identified by two numbers in the topology: (x,y).
The Spinn5 has also 3 FPGA Spartan 6 (FPGA 1 to 3 on the [schemas](#spinn5BoardSchemas)).
Their part is to manage routing with other Spinn5. The Spinn5 board is controlled
by a Board Management Processor (BMP) which manage chips power supply, clocks
and resets for chips and FPGAs.

![](../img/spinnaker_schema.svg){: #spinn5BoardSchemas}

### Connectors

Like suggest with FPGAs, a Spinn5 board coulb be interconnected with other
Spinn5 boards. Each Board has 9 SATA ports (J2, J4, J5, J6, J7, J8, J9, J10 and
J11) which allow interconnections with other boards. This allow to create a
wider the SPiNNaker chip network and create a cluster of Spinn5 boards like
the [one at the Manchester university](introduction.md#spinn5cluster) which is
composed of 600 Spinn5 boards. Having so many SATA ports is usefull because it
help to create a Spinn5 topology that reduce the maximum distance between 2
Spinn5 boards (in terms of wire).

The last connectors types present on the Spinn5 boards is Ethernet. A Spinn5
board has 2 Ethernet connectors. The first one (J14) is use to interact with
the BMP. This administration port allow it's user to alter the board's network
interfaces and manage power supply for SpiNNaker chips. The second one (J16) is
used to manage simulations on computing cores. It is used to monitor SpiNNaker
chips, to load and run softwares on it. An host computer must be connected to
both of them to enable a full use of the board. Moreover, if multiple Spinn5
boards are connected with their SATA ports, only one of them must be connected
to an host computer. Then it will route packets from the host computer to the
right board.

### Others

The Spinn5 board include many LED that show it's state and the state of each
chip.

There is also 2 heat sensor (TN and TS) on the board. Their task is to switch
off the system if the temperature is too high.

# Software Architecture
