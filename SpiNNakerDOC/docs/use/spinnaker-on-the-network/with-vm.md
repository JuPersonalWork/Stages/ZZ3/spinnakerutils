# Why a VM ?

The first time we try to make the SpiNNaker machine usable at the I3S laboratory,
our first idea was to connect it directly to the network. In that case, each
person who wants to use it should have setup his computer with spinnaker
tools like [low level softwares](../low-level-software.md),
[low level C API](../low-level-c-api.md) and
[high level Python API](../high-level-python-api.md). This wasn't a really
good solution because it force each user to follow installation steps for his
own operating system with might be OS X, Windows, Debian base, Red Hat base, ...

Instead we choose create a Debian VM with all tools pre-installed and then
share an export of those VM to any one that have to use it. But manage VM
and VM export might be complicated with a standard hypervisor like VirtualBox.
So we choose to use Vagrant.

# Vagrant

## What is it ?

[Vagrant](https://www.vagrantup.com/) is basically a wrapper of hypervisor. It
provide a unified command line interface that might be plug on VirtualBox,
VMWare, KVM, ...

It as a second interest which is the reason why we choose to use it. Vagrant
provide an easy to use method to manage VM exports and imports.

!!! Warning
    Be careful a VM export from VirtualBox cannot be necessarily import with an
    other hypervisor. The easiest way is to always use virtualbox as wrapped
    hypervisor for use the SpiNNaker management VM.

To add vagrants creators provide a hub of preconfigured VM that are configured
by them and many other contributors. Each VM on that hub should follow some
rules that are:

* ssh server must be configured
* a default user have to be created ( Login: vagrant, Password: vagrant)
* network should be preconfigure : the VM is on the same local network than
the host machine
* a shared folder is connfigured and is, on the vm /vagrant, and on the host
machine the directory in which the VM was configured.

## Install Vagrant and dependencies

### Vagrant

It might happen that your package manager is able to install Vagrant for you. It
isn't recommended by it's developers because it isn't ever up to date. You
should rather go on Vagrant
[web site to download](https://www.vagrantup.com/downloads.html) the latest
version for your operating system and install it. For Windows and Mac OS X it's
easy.

For Debian you should use the following command:

```bash
sudo dpkg -i <download folder>/vargrant_<version numbers>_<architecture 64 or 32>.deb
```

For CentOS you should use the following command:

```bash
rpm -Uvh <download folder>/vargrant_<version numbers>_<architecture 64 or 32>.rpm
```

### VirtualBox

You must also install [VirtualBox]() if it's not already on you computer. On
Windows and Mac OS X, use the installer from
[here](https://www.virtualbox.org/wiki/Downloads).

#### On Debian

On Debian it depends on your OS version. For Debian 8 you can use diretly the
package manager with the command:

```bash
sudo apt-get install virtualbox
```

!!! Warning
    You should have enable contrib softwares installation. If it's not the case,
    you have to modify your /etc/apt/sources.list file which define apt's
    repositories. You only have to add the key word contrib, at the end of each
    repository line like follow:

    Before:</br>
    ```
    deb http://ftp.fr.debian.org/debian/ jessie main 
    deb-src http://ftp.fr.debian.org/debian/ stretch main
    ```

    After:</br>
    ```
    deb http://ftp.fr.debian.org/debian/ stretch main contrib
    deb-src http://ftp.fr.debian.org/debian/ stretch main contrib
    ```

But with Debian 9 it's a little more complicated. In this version virtualbox
has been split from standard repositories and is now on a Debian repository
manage by Oracle which provide VirtualBox. It ensure you that you always have
the latest stable version.

You have 3 steps to follow:

* step 1: add a sources.list file for virtual box:

```bash
echo "deb http://download.virtualbox.org/virtualbox/debian stretch contrib" \
| sudo tee -a /etc/apt/sources.list.d/virtualbox.list
```

* step 2: add the repository public key for security:

```bash
curl -O https://www.virtualbox.org/download/oracle_vbox_2016.asc
sudo apt-key add oracle_vbox_2016.asc
rm oracle_vbox_2016.asc
```

* step 3: update your package manager list and download VirtualBox:

```bash
sudo apt-get update
sudo apt-get install virtualbox-5.1
```

!!! note
    The latest version of VirtualBox is the 5.1 when I write this so replace
    virtualbox-5.1 by the most recent version in the last command.

#### On CentOS

To install VirtualBox on CentOS with it's package manager, follow those steps:

* step 1: add the virtual box repository:

```bash
cd /etc/yum.repos.d
wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo
```

* step 2: install Dynamic Kernel Module Support (DKMS);

```bash
yum --enablerepo=epel install dkms
```

* step 3: install VirtualBox:

```bash
yum install VirtualBox-5.1
```

!!! note
    Same as for Debian 9 : the latest version of VirtualBox is the 5.1 when
    I write this so replace virtualbox-5.1 by the most recent version in the
    last command.



## Some basics to use Vagrant

Now that you know how to install Vagrant and VirtualBox, we can learn how to use
Vagrant. This explanation is a really basic explaination of how to use Vagrant.
It's purpose isn't to be exhaustive but to learn you Vagrant commands that you
have to know to manipulate the SpiNNaker host virtual machine.

As I explain previously, Vagrant provide an easy way to export VM. A VM export's
with Vagrant is called a Box and is an archive with the .box extension. It
contain an export associated with one hypervisor. Our hypervisor is VirtualBox,
so the Box that we have created for hosting the SpiNNaker machine is a
VirtualBox VM. When you have the VM Box (locale path or an URL if it's on a
server) you have to use the following command:

```bash
vagrant box add <path or URL to the Box> <box name on your machine>
```

The last argument is optional but it allow you to give a specific name to the
Box on your computer.

This command import the Box into a local list of Box manage by Vagrant on your
computer. You can list all the Box you got on your computer like that:

```bash
vagrant box list
```

It may help you to check that the new Box is on your computer.

Now we can see the next step. Now that you have a Box on your computer, it's
time to deploy it. The first thing to do is to choose a directory in which
deploy your VM. This directory will not be the one in with your VM virtual
hardrive will be stored, but the one which will be shared with your VM in it's
/vagrant directory. It will also contain a configuration file called Vagrant
file and a configuration subdirectory called .vagrant. The first one allow
you to customize the by modifying the network, the ports binding, ... In a
simple usage only for SpiNNaker I advise you against doing it.

When you have choose this directory go into it. Then type the following command:

```bash
vagrant init <box name on your PC>
```

It will generate the Vagrantfile the .vagrant subdirectory.

Then you can start you're VM (make sure you are still in the same directory than
for the previous command):

```bash
vagrant up
```

If this is the first time you run that command for that Vagrantfile the VM will
be imports from your local Box. Then it will run it, start it's ssh serer. When
it's done you get back the control of your terminal. If there's no error, you
can connect to the VM via SSH:

```bash
vagrant ssh
```

!!! note
    Remember that the user you are connected with as vagrant for login. He as
    the sudo right and it's password is vagrant too.

!!! note
    If you want to launch graphical software on the VM, you have to add a
    parameter to the SSH connection command:</br>
    ```
    vagrant ssh -- -X
    ```

!!! warning
    All argument that you write after the '--' are directly pass to the ssh
    standard command without any check so be careful on what you write.

Now you can do anything you want on the VM. But you might want to do other things
on the VM like stop it or restart it. You can do it from the ssh, but you can
also do that with the Vagrant cli:

```bash
vagrant halt    # stop the VM
vagrant reload  # restart the VM and reload the Vagrantfile
vagrant destroy # destroy the VM : every data that are not in the shared folder will be lost
vagrant help    # show all commands of vagrant
```

For more detailed about Vagrant, [RTFM](https://www.vagrantup.com/docs/index.html).

# Use the spinnaker-board-host VM

Using the VM is not the better configuration to manage the Spinn5 board. But
if you have to manipulate it with you own computer with a direct Ethernet
connection, it's better than install all tools and API.

First you have to get a download address. You can get it in the [download
section](../../download_resources/downloads.md) by right clicking on the
download link and choose "copy link address".

Then use the previously explained command to add the Box into yours, and follows
the step until you're connected on the VM. When you are, you near to be ready to
manipulate the SpiNNaker machine. If you want to use the Python API you have to
update two configurations files in the vagrant user home directory which are
.spiNNakerGraphFrontEnd.cfg and .spynnaker.cfg. Each of those files has a line
with those form : "machineName  = spiky.i3s.unice.fr". Update the value to
be the actual IP address or domain name of the Spinn5 board (not the host server,
directly the board).

The vagrant user home dire contain also two useful symbolic links. One on the
shared directory called "shared" and one the SpiNNaker C API tests programs
called "SpiNNakerApp".

# Constraints

The problem to use the VM and have the SpiNNaker on the I3S laboratory network
is that it use the UDP IP protocol for communications between the host machine
and the board. As you may know, UDP, despite of TCP, does not use any
acknowledgement and many network packets might be lost because of the traffic.
