# Why a server ?

When we discover the problem about UDP protocol to communicate with the
Spinn5 board on the I3S network, we search a viable alternative. The solution
comes from the Manchester Team which explain us that they use an host server
on their SpiNNaker cluster to control an use it. So we decide to use an old
computer to act as our SpiNNaker host server. It must fulfill one condition
which is to get 2 network interfaces. The first one is used to connect the
server on the I3S laboratory network. The second one is connected to a network
switch on which their is only 3 network interfaces that are connected: the
server's one, and the Spinn5 board's 2 Ethernet interfaces.

In that way the network on which the host server communicate with the Spinn5
board is dedicate to that communication. Only network packet for that
communication are there, which make that there is almost no packet lost.

# How to install it ?

We choose to use Debian as our server Operating System. You'll find a tutorial
of how to install Debian
[here](https://pixel-isima.github.io/tutorial/2017/01/17/Dual-Boot-Debian.html)

Then you have to run all the 3 scripts describes in the
[setup environment part](../setup-environment.md).

# Configurations

Now that all tools you need are ready on the server, you have to setup some
configuration files.

The first file you have to setup is the network file `/etc/network/interfaces`.
We already explain that the computer should have two Ethernet interfaces. You
have to know how your system call both of them. In our first installation, eth1
was connected to the Spinn5 board, and eth0 was connected to I3S network.
so the network configuration files look like this :

```bash
# The loopback network interface
auto lo
iface lo inet loopback

# The I3S network interface
auto eth0
iface eth0 inet static
	address 134.59.131.179
	netmask 255.255.255.0
	gateway 134.59.131.254

# The SpiNNaker network interface
auto eth1
iface eth1 inet static
	address 192.168.240.1
	netmask 255.255.255.0
	gateway 192.168.240.254
```

Noticed that at that time the Spinn5 board network interface was:

```bash
# Simulation interface
spiky-bmp:0 > spin_ip
Flag: c059
MAC:  00:00:a4:00:3e:88
IP:   192.168.240.3
GW:   192.168.240.254
NM:   255.255.255.0
Port: 17893

# BMP interface
spiky-bmp:0 > bmp_ip
Flag: c000
MAC:  00:00:a4:00:3e:87
IP:   192.168.240.2
GW:   192.168.240.254
NM:   255.255.255.0
Port: 17893
```

The sever interface on the SpiNNaker network should have an IP address and a
gateway that fit with the SpiNNaker interfaces one's and mask.

The next two files that you need to setup are the same
as in the VM way: .spiNNakerGraphFrontEnd.cfg and .spynnaker.cfg. Each of those
files has a line with those form : "machineName  = spiky.i3s.unice.fr". Update
the value to be the actual IP address or domain name of the Spinn5 board on the
local network shared between the board and the server.

When It's done you are ready to work with the server. I advise you to connect
to it via ssh. The most secure way is to use only ssh keys to connect to the
server and no password.