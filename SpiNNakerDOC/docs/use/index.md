# Use the I3S laboratory's SpiNNaker machine

Here we will see how to use the I3S lab SpiNNaker machine :

* [low level software](low-level-software.md)
* [low level C API](low-level-c-api.md)
* [high level Python API](high-level-python-api.md)
* [create a custom neuron model](custom-neuron-model.md)
* [tools around SpiNNaker](tools-around-spinnaker.md)

But before all, you should learn how to make the Spinn5 board usable in the
I3S network. There is two way to do that which are with a
[VM](spinnaker-on-the-network/with-vm.md) or with a
[host server](spinnaker-on-the-network/with-host-server.md).