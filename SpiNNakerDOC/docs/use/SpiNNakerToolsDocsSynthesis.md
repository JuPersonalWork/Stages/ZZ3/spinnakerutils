SpiNNaker Tools docs synthesis
===

SARK : Applications cores
SC&MP : Monitor core

Interesting tools are :
* gdb-spin : debug tool
* sark : applications core low level API
* scp : Spinnaker Command Protocol
* spin1_api : Spinnaker API which implement an Event-Driven programming model.
* ybug : System Control Tool for SpiNNaker.

## SARK : SpiNNaker Application Runtime Kernel

### Introduction

De manière assez simpliste, SARK est l'API de développement C la plus bas niveau
pour les programmes applicatifs. Cette API rempli trois fonctions principales :
* elle initialise le coeur ARM chargé d'exécuté l'application en préparant ses
  registres puis ensuite elle appel le fonction main (point d'entrée) de
  l'application.
* elle fourni des des fonctions bah niveau de gestion de memoire,
  d'interruptions, ...
* elle met en place des mecanismes de communication entre le processeur moniteur
  et les processeurs applicatifs en utilisant des paquet SDP. Ces mecanismes
  permettent de piloter les application à partir d'une machine qui sert
  d'hôte au hardware SpiNNaker.

### Chargement d'application et démarrage

Une application SpiNNaker est un programme qui s'exécute sur un seul coeur et
est le seul à s'y exécuter jusqu'à son arrêt final. Une même application peut
être chargée et exécutée sur plusieur coeurs à la fois. Par exemple une
application Neuron sera exécutée sur plusieurs coeurs pour créer un réseau.
Une application SpiNNaker codé en C avec SARK doit être compilé au format APLX
par un compilateur corssplatform afin de pouvoir être chargé dans un coeur de
SpiNNaker. Dans le cas géneral, l'application est chargé dans la mémoire
partagée d'une puce par le coeur moniteur. Puis ce dernier la charge ensuite
dans chacun des coeurs choisis pour l'exécution.

### L'environnement d'excécution

Les coeurs utilisé dans les machines SpiNNaker sont des coeur ARM968 avec une
architecture Harvard (séparation entre mémoire de stockage des données, ici
DTCM, et mémoire de stockage du programme, ici ITCM). Les données et
instructions processeurs doivent bien restées dans ces mémoire spécifiques car
leurs accès est très rapide (un cycle d'horloge soit ~ns) là où l'accès à la
sysRAM ou à la SDRAM est à fois plus long.

### Le démarrage de SARK

cf. doc pdf sur SARK

### Utilisation

Pour utiliser l'API SARK, il faut inclure le header sark.h qui contient la
définitions des fonctions de SARK et inclus le header spinnaker.h qui contient
les définitions Hardware (TODO check spinnaker.h).

L'API SARK fourni un panel assez complet de fonctions bah niveaux pour gérer le
système SpiNNaker:
* gestion direct des cpu et de leurs registre (activation, desactivation,
  mutateur de registre, ...).
* routines de manipulation de la mémoire (copy, calcule de longueur de donnée,
  ...).
* génération de nombre aléatoire 32 bits ( TODO je ne pense pas qu'il soit de
  très bonne qualité, à vérrifier).
* Gestion de messages SDP (SpiNNaker Data Protocol), allocation de buffers dans
  la mémoire DTCM dédiés au messages.
* routines de manipulation d'erreurs.
* routines d'affichage de texte dans un buffer de sortie.
* gestions de verrous et semaphores.
* gestion de mémoire, allocation et libérations, pour les mémoire DTCM, SDRAM et
  System RAM.
* gestion hardware : routines de manipulation des leds, du VIC (Vectored
  Interrupt Controller), de mise en attente et de control des tables de
  routages.
* gestion du temps avec la programmations d'evennements.
* routines de transmissions de paquets.
* routines de gestion d'evennements pour lancer et stoper leurs exécutions, ou
  encore les placer en file d'attente.

Cette API fournie également des variables de structures de données spécifiques
contenants des informations intéressantes pour les applications :
* Une structure de type sark_vec_t est mise à disposition en variable global et
  contient des informations sur la taille et la positions des piles
  d'exéccutions, des buffers alloués, ,... dans la mémoire.
* Une structure de type sark_data_t est allouée dans la mémoire DTCM et
  accessible dans la variable globale __sark__. Elle contient des pointeur sur
  des structure de donné utils telles que le tas dans la mémoire DTCM, ou les
  buffer spécifique au coeur applicatif utilisé dans la SDRAM et la System RAM.
* Une structure de type event_data_t, présente également dans la mémoire DTCM,
  contient l'état du gestionnaire d'evennement si celui-ci est utilisé.
* Une structure de type vcpu_t présente dans la System RAM accessible via la
  variable globale __sark__ : __sark.vcpu__. Elle contient des données qui
  doivent être accessibles aux autres coeurs et en particulier le coeur
  moniteur. Certain attributs sont utilisé pour les echange de messages,
  d'autres sont utilisé pour le débogage d'applications.

La plupart de ces structures de données sont réinitialisés au démarrage d'une
application.
Par ailleur, chaque coeur dispose d'un buffer privé dans la SDRAM de 128KB.
Il est accessible via la variable global __sark__ : __sark.sdram_buf__.

il existe également des variable à l'echelle du système, telles que le tas dans
la SDRAM ou celui de la System RAM et un ensemble d'autres variables présentes
dans la System RAM. En général, cést le coeur moniteur qui se charge de les
initialiser et de les modifier. Ces variables systèmes sont contenue dans une
structure de donnée de type sv_t et accessible dans la varible globale __sv__.
