README
===

You want to deploy this VM to use and manage the I3S Labs' Spinn_5 48 node
board ? You're at the right place.

# Setup tools

To deploy such a VM you need Vagrant and VirtualBox.

## Vagrant

You need to download the latest version for your Operating System from the following web
page : [https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html)

Then use your OS standard installation process to install it.

## VirtualBox

Depending on your operating system and package manager you might find VirtualBox
directly with your package manager :

(Debian/Ubuntu)
> sudo apt-get install virtualbox

You can also download VirtualBox from the official website :
[https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

# Use Vagrant to deploy the VM

This README simply presents the process to deploy the VM and does not provide an
exhaustive Vagrant or VirtualBox documentation.

To provide a VM Vagrant uses something called Box. A box is a kind of archive
which contains a VM export for a VM provider (VirtualBox, KVM, VMware, ...) and
some config files that are helpful to Vagrant to configure the VM. So what
you have to do first is to import the Vagrant boxes of that VM onto your
Computer :

> vagrant box add <box name> <box url or relative path or name on the vagrant hub>

Example:

> vagrant box add spinnaker-board-host http://dhcp129-226.i3s.unice.fr:10080/spinnaker-board-host.box

Then choose a directory in which the VM configuration is initialized. I'll choose
'~/spinnaker-board-host'

> mkdir ~/spinnaker-board-host

Then go into it and run the following command to initialize the Vagrant config :

> cd ~/spinnaker-board-host</br>
> vagrant init spinnaker-board-host

When it's done you can then run the VM with the following command :

> vagrant up

Now you've got a debian 8 VM with useful SpiNNaker tools that run in background
without any GUI.

The last thing you have to do to work with it is to connect to the VM with ssh.
Vagrant provides a way to do it easily :

> vagrant ssh

If you want to run graphical app from the VM you should run the ssh connection as follows :

> vagrant ssh `--` -X

__NOTE__: The `--` allows you to pass parameters to the ssh command and directly
to Vagrant.

If you want to stop the VM run:

> vagrant halt

For more information about Vagrant cli use:

> vagrant help

For more information about Vagrant, [RTFM](https://www.vagrantup.com/docs/index.html)
