# Before all

When you want to setup an environment to use the SpiNNaker machine you have
to wonder what you want to do. Let me explain. We have created 3 setup
script for 3 different use of SpiNNaker machine. The first one install the
[C API](low-level-c-api.md) and the [low-level-tools.md]. The second one install
the [Python API](high-level-python-api.md). Their use is relatively independent
which make you able to run only one of them to use what it has installed.
The last one needs you to run the two previous ones before. It's purpose is to
download and setup libraries for making possible the creation of [custom neural
models](custom-neuron-model.md) on SpiNNaker


for low level tools :

Looks like you just need to install the Perl module. To do this, login to SSH as root and type:

perl -e shell -MCPAN

If it asks you to set it up, just type "no" and it'll use the defaults.

cpan> install String::CRC32

Running that command should install the missing Perl module.