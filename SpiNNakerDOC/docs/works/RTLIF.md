Details about RTLIF in goddess
===

## Attributs
nom | valeurs au cours de la simulations | description
---|---|---
**_state** | ]-&infin;,+&infin;[ | Il s'agit de la représentation du potentiel de membrane du modèle de neurone. Dans le cas de cette implémentation, il est compris entre -&infin; et +&infin;
**_input** | ]-&infin;,+&infin;[ | Il s'agit de la sommes des potentiels des ports d'entrés du modèle.
**_axon** |{0,1}| Cet attribut sert de booléen de sortie. Dans le modèle courant, lorsque celui-ci "spike", il emet toujours le même potentiel (1) et il suffit donc de dire que la sortie du modèle qui est routée vers le port de sortie est de 0 ou de 1.
**_delay** |1.0| C'est le retard de "transport" de l'activité des entrée du neurone  (synapse) vers le neurone.
**_epsilon** | 0.1 | Valeur en dessous de laquelle l'état (\_state) est considéré comme valant 0.
**_weights** | {</br>&emsp;"in": 1.0</br>} | Map qui associe des poids à des ports d'entrée en utilisant leurs noms.
**_r** | 0.8 | Constante de décroissance exponentielle.
**_threshold** | 1.0 | Seuil d'activation du neurone. Il s'agit de la valeur de \_state au delà de laquelle le neurone est considéré comme activé et émet un spike avant de retomber à un \_state de 0.

## Méthodes

### &delta;<sub>int</sub>

#### Algorithme

```python
def deltaInt():
  if _phase == "active"
    _state = 0
  else if  _phase == "transport"
    _state *= _r^_delay # Leakage
    _state += _input

  if _state >= _threshold
    _axon = 1
    _state = 0
    addEvent("firing", 500.0)
  else if _state > _epsilon
    _axon = 0
    addEventWithoutOutput("active", ln(_epsilon/|_state|)/ln(_r)+1000)
  else
    _axon = 0
    passivate()
```

### &delta;<sub>ext</sub>

#### Algorithme

```python
def deltaExt(elapseTime, messages):
  if elapseTime > 0
    _state *= _r^elapseTime # Leakage

  for message in messages
    _input += message.value * message.origin.weight # message.origin.weight est le poids de la synapse courante

  addEventWithoutOutput("transport", _delay*500)
```

### &delta;<sub>con</sub>

#### Algorithme

```python
def deltaCon(elapseTime, messages):
  deltaExt(elapseTime, messages)
  deltaInt()
```

## Représentation en Atomic DEVS

RTLIF = (X, Y, S, ta, &delta;<sub>ext</sub>, &delta;<sub>inf</sub>, &lambda;) :

Variables| Values
---|---
X|]-&infin;,+&infin;[
Y|{0,1}
S|{(d,&sigma;) \| d &isin; {"passive", "active", "transport", "firing"}, &sigma; &isin; T<sup>&infin;</sup>}
s<sub>0</sub>|("passive", +&infin;)
ta(s) | &sigma; &forall; s &isin; S
&delta;<sub>ext</sub>(((phase, &sigma;),t<sub>e</sub>),x) &forall; (phase,&sigma;) &isin; S|("transport",\_delay*500)
&delta;<sub>inf</sub>("active",&sigma;)| ("passive", &infin;)
&delta;<sub>inf</sub>(phase,&sigma;) &forall; phase &isin; {"passive","transport","firing"}| ("firing", 500.0) &forall; \_state &isin; [\_threshold, +&infin;[ </br> ("active", ln(\_epsilon/\|\_state\|)/ln(\_r)+1000) &forall; \_state &isin; ]\_epsilon,\_threshold[ </br> ("passive", +&infin;) &forall; \_state &isin; [0, \_epsilon]
&lambda;("firing", &sigma;)|1
&lambda;(phase, &sigma;) &forall; phase &isin; {"passive", "active", "transport"}|0

sachant que :
* transport : de mémoire c'est la phase avec délai de transport du potentiel des
  dendrites au soma

* firing : le temps d'émission du spike depuis le soma jusqu'à l'axone

* active : potentiel de soma >= epsilon
