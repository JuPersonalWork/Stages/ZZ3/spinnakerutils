# SpiNNaker at the I3S Lab

Welcome on the documentation about SpiNNaker at the I3S laboratory. This
site has 3 goal :

* Present the SpiNNaker Project and the SpiNNaker machine ([here](presentation/introduction.md)).
* Explain how to use the SpiNNaker machine owned by the I3S laboratory ([here](use/index.md)).
* Present all works that has been done on that machine ([here](works/index.md)).

![](img/spinnaker_i3s.JPG)